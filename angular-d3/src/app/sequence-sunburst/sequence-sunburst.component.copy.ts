import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

type SequenceData = {
  name: string;
  value: number;
};

@Component({
  selector: 'app-sequence-sunburst',
  templateUrl: './sequence-sunburst.component.html',
  styleUrls: ['./sequence-sunburst.component.css']
})



export class SequenceSunburstComponent implements OnInit {

  constructor() { }
  private breadcrumbWidth: number = 75;
  private breadcrumbHeight: number =  30;
  private radius: number = 100;

  private buildHierarchy(csv: any): any {
    // Helper function that transforms the given CSV into a hierarchical format.
    const root = { name: "root", children: [] };
    for (let i = 0; i < csv.length; i++) {
      const sequence = csv[i][0];
      const size = +csv[i][1];
      if (isNaN(size)) {
        // e.g. if this is a header row
        continue;
      }
      const parts = sequence.split("-");
      let currentNode: any = root;
      for (let j = 0; j < parts.length; j++) {
        const children = currentNode["children"];
        const nodeName = parts[j];
        let childNode = null;
        if (j + 1 < parts.length) {
          // Not yet at the end of the sequence; move down the tree.
          let foundChild = false;
          for (let k = 0; k < children.length; k++) {
            if (children[k]["name"] == nodeName) {
              childNode = children[k];
              foundChild = true;
              break;
            }
          }
          // If we don't already have a child node for this branch, create it.
          if (!foundChild) {
            childNode = { name: nodeName, children: [] };
            children.push(childNode);
          }
          currentNode = childNode;
        } else {
          // Reached the end of the sequence; create a leaf node.
          childNode = { name: nodeName, value: size };
          children.push(childNode);
        }
      }
    }
    return root;
  }
  private partition(data: SequenceData):any{    
    return d3.partition().size([2 * Math.PI, this.radius * this.radius])(
      d3
        .hierarchy(data)
        .sum(d => d.value)
        .sort((a:any, b:any) => b.value - a.value)
    )
  }

  private breadcrumbPoints(d: any, i: any): string {
    const tipWidth = 10;
    const points = [];
    points.push("0,0");
    points.push(`${this.breadcrumbWidth},0`);
    points.push(`${this.breadcrumbWidth + tipWidth},${this.breadcrumbHeight / 2}`);
    points.push(`${this.breadcrumbWidth},${this.breadcrumbHeight}`);
    points.push(`0,${this.breadcrumbHeight}`);
    if (i > 0) {
      // Leftmost breadcrumb; don't include 6th vertex.
      points.push(`${tipWidth},${this.breadcrumbHeight / 2}`);
    }
    return points.join(" ");
  }
  ngOnInit(): void {
    d3.json("http://localhost:4200/assets/pie-config.json").then((config: any)=>{
      d3.json("http://localhost:4200/assets/sequence-data.json").then((data: any)=>{
        let width = 640
        let radius = width / 2
        const arc = d3
          .arc()
          .startAngle((d:any) => d.x0)
          .endAngle((d:any) => d.x1)
          .padAngle(1 / radius)
          .padRadius(radius)
          .innerRadius((d:any) => Math.sqrt(d.y0))
          .outerRadius((d:any) => Math.sqrt(d.y1) - 1)
        const color = d3
          .scaleOrdinal()
          .domain(["home", "product", "search", "account", "other", "end"])
          .range(["#5d85cf", "#7c6561", "#da7847", "#6fb971", "#9e70cf", "#bbbbbb"]) as any
        const mousearc = d3
          .arc()
          .startAngle((d:any) => d.x0)
          .endAngle((d: any) => d.x1)
          .innerRadius((d: any) => Math.sqrt(d.y0))
          .outerRadius(radius)
        data = this.buildHierarchy(data)
        const root = this.partition(data);
        const svg = d3.create("svg");
        // Make this into a view, so that the currently hovered sequence is available to the breadcrumb
        const element = svg.node() as any;

        element!.value = { sequence: [], percentage: 0.0 };

        const label = svg
          .append("text")
          .attr("text-anchor", "middle")
          .attr("fill", "#888")
          .style("visibility", "hidden");

        label
          .append("tspan")
          .attr("class", "percentage")
          .attr("x", 0)
          .attr("y", 0)
          .attr("dy", "-0.1em")
          .attr("font-size", "3em")
          .text("");

        label
          .append("tspan")
          .attr("x", 0)
          .attr("y", 0)
          .attr("dy", "1.5em")
          .text("of visits begin with this sequence");

        svg
          .attr("viewBox", `${-radius} ${-radius} ${width} ${width}`)
          .style("max-width", `${width}px`)
          .style("font", "12px sans-serif");

        const path = svg
          .append("g")
          .selectAll("path")
          .data(
            root.descendants().filter((d:any) => d.depth && d.x1 - d.x0 > 0.001)
          )
          .join("path")
          .attr("fill", (d:any):string => color(d.data.name))
          .attr("d", arc.toString());

      svg
        .append("g")
        .attr("fill", "none")
        .attr("pointer-events", "all")
        .on("mouseleave", () => {
          path.attr("fill-opacity", 1);
          label.style("visibility", "hidden");
          // Update the value of this view
          element.value = { sequence: [], percentage: 0.0 };
          element.dispatchEvent(new CustomEvent("input"));
        })
        .selectAll("path")
        .data(
          root.descendants().filter((d:any) => {
            // Don't draw the root node, and for efficiency, filter out nodes that would be too small to see
            return d.depth && d.x1 - d.x0 > 0.001;
          })
        )
        .join("path")
        .attr("d", mousearc.toString())
        .on("mouseenter", (event, d:any) => {
          // Get the ancestors of the current segment, minus the root
          const sequence = d
            .ancestors()
            .reverse()
            .slice(1);
          // Highlight the ancestors
          path.attr("fill-opacity", node =>
            sequence.indexOf(node) >= 0 ? 1.0 : 0.3
          );
          const percentage = ((100 * d.value) / root.value).toPrecision(3);
          label
            .style("visibility", null)
            .select(".percentage")
            .text(percentage + "%");
          // Update the value of this view with the currently hovered sequence and percentage
          element.value = { sequence, percentage };
          element.dispatchEvent(new CustomEvent("input"));
        });

      return element;
    })
  })
}
}