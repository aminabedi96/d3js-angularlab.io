import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

type BarData = {
  name: string;
  value: number;
};


@Component({
  selector: 'app-sortable-bar',
  templateUrl: './sortable-bar.component.html',
  styleUrls: ['./sortable-bar.component.css']
})

export class SortableBarComponent implements OnInit {
  private data: BarData[];
  private svg: any;
  private x: any;
  private y: any;
  private gx: any;
  private gy: any;
  private xAxis: any;
  private yAxis: any;
  private bar: any;



  constructor() { 
    this.data = []
  }

  private createSvg(height: number,
                    width: number,
                    margin: {left: number, right: number, bottom: number, top: number},
                    fillColor: string,
          ): void {
    console.log(height, width, margin, fillColor)
    this.x = d3.scaleBand()
        .domain(this.data.map(d => d.name))
        .range([margin.left, width - margin.right])
        .padding(0.1)
    this.y = d3.scaleLinear()
        .domain([0, d3.max(this.data.map(d => d.value)) as number])
        .nice()
        .range([height - margin.bottom, margin.top])

    console.log(this.y(0), this.y(0.1), this.y(.5), this.y(1), [0, d3.max(this.data, d => d.value)], [height - margin.bottom, margin.top])

    this.xAxis = (g: any) => g
        .attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(this.x).tickSizeOuter(0))

    this.yAxis = (g: any) => g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(this.y))
        .call((g: any) => g.select(".domain").remove())

    this.svg = d3.select("figure#sortable-bar").append("svg")
        .attr("viewBox", ["0", "0", width, height].toString());

    this.bar = this.svg.append("g")
        .attr("fill", fillColor)
        .selectAll("rect")
        .data(this.data)
        .join("rect")
        .style("mix-blend-mode", "multiply")
        .attr("x", (d:any) => this.x(d.name))
        .attr("y", (d:any) => this.y(d.value))
        .attr("height", (d:any) => this.y(0) - this.y(d.value))
        .attr("width", this.x.bandwidth());

    this.gx = this.svg.append("g")
        .call(this.xAxis);

    this.gy = this.svg.append("g")
        .call(this.yAxis);
  }


  onOrderSelected(order: string){
    console.log("Selected", order)
    let orderFn: (a: BarData, b: BarData)=>number= ()=>0;
    if(order == "Alphabet"){
      orderFn = (a:BarData,b: BarData) => {
        return d3.ascending(a.name, b.name)
      }
    }
    else if(order == "DESC"){
      orderFn = (a:BarData,b: BarData) => {
        console.log("Ordering", a, b)
        return d3.descending(a.value, b.value)
      }
    }
    else if(order == "ASC"){
      orderFn = (a:BarData,b: BarData) => {
        return d3.ascending(a.value, b.value)
      }
    }
    console.log({orderFn})
    this.x.domain(this.data.sort(orderFn).map(d => d.name));
    const t = this.svg.transition()
        .duration(750);

    this.bar.data(this.data, (d: any) => d.name)
        .order()
        .transition(t)
        .delay((d: any, i: any) => i * 20)
        .attr("x", (d: any) => this.x(d.name));

    this.gx.transition(t)
        .call(this.xAxis)
        .selectAll(".tick")
        .delay((d: any, i: any) => i * 20);
  }
  private createOptions(): void {
    d3.select("#order")
        .selectAll("option")
        .data(["Alphabet", "ASC", "DESC"])
        .enter()
        .append("option")
        .attr("value", d => d)
        .text(d => d)
  }

  ngOnInit() {
    this.createOptions()
    d3.json("http://localhost:4200/assets/bar-config.json").then((config: any)=>{
      d3.json("http://localhost:4200/assets/bar-data.json").then((data: any)=>{
        this.data = data
        let {height, width, margin, fillColor} = config
        this.createSvg(height, width, margin, fillColor);
      })
    })
    
  }

}
